
import pandas as pd

health_2017 = pd.read_csv('NHIS_OPEN_GJ_2017.csv', header=0, index_col=None, encoding='cp949')

health_2017 = health_2017.iloc[:, 2:7]

health_2017.rename(columns=
                   {'성별코드': 'Sex_code',
                    '연령대코드(5세단위)': 'Age_code',
                    '시도코드': 'Address_code',
                    '신장(5Cm단위)': 'Height',
                    '체중(5Kg 단위)': 'Weight'}, inplace=True)

# health_not_age = health_2017.drop(['Age_code'],axis=1)
#
# health_not_age.head()

health_2017.head()


bmi_df = pd.DataFrame(columns={'Address_code', 'Sex_code', 'bmi'})

i = 0

for height in health_2017.iloc[:, 3]:
    bmi = health_2017.iloc[i, 4] / pow(height, 2)
    bmi_list = [health_2017.iloc[i, 2], health_2017.iloc[i, 0], bmi]
    bmi_df.append(bmi_list)
    i = i + 1

bmi_df.head()

bmi_df.to_csv('2017_bmi.csv', index_label=None, header=0)