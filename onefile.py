
import pandas as pd
import os
import glob

all_data_set = []

all_files = glob.glob(os.path.join(os.getcwd(), 'NHIS*.csv'))

for file in all_files:
    data_frame = pd.read_csv(file, index_col=None, engine='python',header =0)
    all_data_set.append(data_frame)


all_data_concated = pd.concat(all_data_set, axis=0 ,ignore_index=True)

all_data_concated.to_csv('all_health.csv',index=None,encoding='cp949')